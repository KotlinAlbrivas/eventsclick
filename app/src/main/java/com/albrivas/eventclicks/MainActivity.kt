package com.albrivas.eventclicks

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import com.google.android.material.button.MaterialButton

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var btnXML : MaterialButton
    private lateinit var btnLine : MaterialButton
    private lateinit var btnMulti : MaterialButton
    private lateinit var btnMulti2 : MaterialButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        instancias()
        acciones()

        // Hay que llamarlo aqui para poder ejecutarlo
        onCLickLine()
    }

    private fun acciones() {
        btnMulti.setOnClickListener(this)
        btnMulti2.setOnClickListener(this)
    }

    private fun instancias() {
        btnXML = findViewById(R.id.buttonXML)
        btnLine = findViewById(R.id.buttonLine)
        btnMulti = findViewById(R.id.buttonMulti)
        btnMulti2 = findViewById(R.id.buttonMulti2)
    }

    // La funcion debe ser publica y hay que pasarle un solo parametro de tipo View
    fun onClickXML(view : View) {
        sendMessage("Boton XML pulsado")
    }

    private fun onCLickLine(){
        btnLine.setOnClickListener {
            sendMessage("Boton In Line pulsado")
        }
    }

    // Implementado como interfaz
    override fun onClick(v: View) {
        when(v.id) {
            R.id.buttonMulti -> sendMessage("Boton multiline pulsado")
            R.id.buttonMulti2 -> sendMessage("Boton multiline 2 pulsado")
        }
    }

    private fun sendMessage(message: String) {
        Toast.makeText(this,  message, Toast.LENGTH_SHORT).show()
    }
}
